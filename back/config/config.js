require('dotenv').config();

module.exports = {
  "development": {
    "username": "seosado",
    "password": process.env.MYSQL_PASSWORD,
    "database": "slackclone",
    "host": process.env.MYSQL_HOST,
    "dialect": "mysql",
    "port":process.env.MYSQL_PORT
  },
  "test": {
    "username": "seosado",
    "password": process.env.MYSQL_PASSWORD,
    "database": "slackclone",
    "host": process.env.MYSQL_HOST,
    "dialect": "mysql",
    "port":process.env.MYSQL_PORT
  },
  "production": {
    "username": "seosado",
    "password": process.env.MYSQL_PASSWORD,
    "database": "slackclone",
    "host": process.env.MYSQL_HOST,
    "dialect": "mysql",
    "port":process.env.MYSQL_PORT
  }
}
